delegate = function(func, ...)
  local curState = state -- save in case it changed in delegated method
  for _, mod in ipairs(modules) do
    local ok = mod.state
    if ok then
      ok = false
      if #mod.state == 0 then ok = true end
      for _, st in ipairs(mod.state) do
        if curState == st then ok = true end
      end
    end
    if ok and mod.obj[func] then
      mod.obj[func](...)
    end
  end
end

loadModules = function(table)
  modules = table
  for _, mod in ipairs(modules) do
    _G[mod.id] = {}
    require(mod.id)
    mod.obj = _G[mod.id]
    if mod.obj.__constructor then
      mod.obj.__constructor()
    end
  end
end

-- load classes. initializes "meta" field for the object returned
loadClasses = function(...)
  for i = 1, select('#', ...) do
    local class = select(i, ...)
    local obj = require("classes/" .. class)
    if type(obj) ~= "table" then
      print("class", class, "has incorrect returned ", obj, "expected table")
    end
    obj.meta = { __index = {} }
    for k, v in pairs(obj) do
      obj.meta.__index[k] = v
    end
    _G[class] = obj
  end
end

push = function(arr, value)
  arr[#arr + 1] = value
end
