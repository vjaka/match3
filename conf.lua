config = {
  debug = true,

  match = 3,                -- (3) minimal amount of tiles that forms a figure to disappear
  specialMatch = 4,         -- (4) how many tiles matched at the same time should convert to HOR/VER
  tiles = 6,                -- (6) # of different tiles in game
  cols = 8,                 -- (8) maximal game width
  rows = 8,                 -- (8) maximal game height
  suggestCostScore = 10,    -- cost of using suggest in score
  suggestAnimTime = 0.5,    -- time of single suggest animation
  suggestAnimRepeat = 3,    -- how many times to repeat suggest animation

  anim = {
    slow_k = 2.50,          -- slow animation play K
    fast_k = 0.50,          -- fast animation play K
    initial_k = 0.2,        -- animation K when initially showing the board
    
    move_1cell = 0.25,      -- time of a cell move for 1 tile
    drop_1cell = 0.09,      -- time to drop a cell for 1 tile
    spawn_1cell = 0.20,     -- time to spawn a cell for 1 tile
    disappear = 0.40,       -- time to disappear
    tile_anim = 0.10,       -- speed of animation changing within the tile

    initialAnimation = false, -- whatever the field should be animated initially like just everything was erased
  },

  score = {                 -- scores for matching specified amount of tiles in a row
    [1] = 3,                -- values < [match] caused by special effects
    [2] = 6,
    [3] = 10,
    [4] = 15,
    [5] = 25,
    [6] = 40,
    [7] = 90,
    [8] = 120,
  },

  ui = {
    wmenu = 0.25,           -- width of menu (%) of game screen
  },
}

-- love2d engine-specific configuration
love.conf = function(t)
  t.window.resizable = true

  -- disable not used modules
  t.modules.joystick = false
  t.modules.physics = false
end
