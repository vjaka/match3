require("conf")

options.onKeyPress = function(key)
  if key == "escape" then
    scene.change("menu")
  end
end

options.onOpen = function()
  ui.button.add("exit",
    function()
      local menuPart = width * config.ui.wmenu
      local exitW = menuPart*0.6
      local exitH = (level.gfxMenu:getHeight() / level.gfxMenu:getWidth()) * exitW
      return { x1 = 0, y1 = height - exitH, x2 = exitW, y2 = height }
    end,
    function(rect)
      love.graphics.draw(level.gfxMenu, rect.x1, rect.y1, 0,
        (rect.x2-rect.x1)/level.gfxMenu:getWidth(), (rect.y2-rect.y1)/level.gfxMenu:getHeight())
    end,
    function()
      scene.change("menu")
    end)

  ui.button.add("changeAnimSpeed",
    function()
      return { x1 = width/4, y1 = height/6, x2 = width/4*3, y2 = height/6*2 }
    end,
    {function()
      if level.animK == config.anim.slow_k then
        return "Animation speed: Slow"
      elseif level.animK == config.anim.fast_k then
        return "Animation speed: Fast"
      else
        return "Animation speed: Normal"
      end
    end},
    function()
      if level.animK == config.anim.slow_k then
        level.animK = 1.0
      elseif level.animK == config.anim.fast_k then
        level.animK = config.anim.slow_k
      else
        level.animK = config.anim.fast_k
      end
    end)

  ui.button.add("toggleInitialAnimate",
      function()
          return { x1 = width / 4, y1 = (height / 6)*2, x2 = width / 4 * 3, y2 = (height / 6) * 2 + (height/6) }
      end,
      {
          function()
              if config.anim.initialAnimation then
                  return "Animate initial table generation"
              else
                  return "Instantly show initial table"
              end
          end
      },
      function()
          config.anim.initialAnimation = not config.anim.initialAnimation
      end)
end
