-- 0: no field here
-- 1: is a spawning field
-- 2..N: other fields in order how tile from spawning field should drop
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },
--    { 0, 0, 0, 0, 0, 0, 0, 0 },


scenario.campaign = {
  --[[{
    field = {
      { 1, 1, 1, 1, },
      { 2, 2, 2, 2, },
      { 3, 3, 3, 3, },
      { 4, 4, 4, 4, },
      { 5, 5, 5, 5, },
      { 6, 6, 6, 6, },
    },
    board = {
      { 1, 1, 2, 1, },
      { 2, 2, 3, 2, },
      { 1, 3, 2, 3, },
      { 4, 1, 5, 2, },
      { 1, 5, 2, 5, },
      { 1, 1, 2, 1, },
    },
  },--]]
  {
    field = {
      { 0, 0, 0, 0, 0, 0, 0, 0 },
      { 0, 1, 1, 1, 1, 1, 1, 0 },
      { 0, 2, 2, 2, 2, 2, 2, 0 },
      { 0, 3, 3, 3, 3, 3, 3, 0 },
      { 0, 4, 4, 4, 4, 4, 4, 0 },
      { 0, 5, 5, 5, 5, 5, 5, 0 },
      { 0, 6, 6, 6, 6, 6, 6, 0 },
      { 0, 0, 0, 0, 0, 0, 0, 0 },
    },
    moves = 30,
    score = 300,
  },
}

scenario.free = {
  { -- first mission will be suggested on a freeplay mode
    field = {
      { 1, 1, 1, 1, 1, 1, 1, 1 },
      { 2, 2, 2, 2, 2, 2, 2, 2 },
      { 3, 3, 3, 3, 3, 3, 3, 3 },
      { 4, 4, 4, 4, 4, 4, 4, 4 },
      { 5, 5, 5, 5, 5, 5, 5, 5 },
      { 6, 6, 6, 6, 6, 6, 6, 6 },
      { 7, 7, 7, 7, 7, 7, 7, 7 },
      { 8, 8, 8, 8, 8, 8, 8, 8 },
    },
    title = "REGULAR",
  },
  {
    field = {
      { 0, 0, 5, 6, 6, 5, 0, 0 },
      { 0, 3, 4, 5, 5, 4, 3, 0 },
      { 1, 2, 3, 4, 4, 3, 2, 1 },
      { 1, 2, 3, 4, 4, 3, 2, 1 },
      { 1, 2, 3, 4, 4, 3, 2, 1 },
      { 1, 2, 3, 4, 4, 3, 2, 1 },
      { 0, 3, 4, 5, 5, 4, 3, 0 },
      { 0, 0, 5, 6, 6, 5, 0, 0 },
    },
    title = "SIDE MIRROR",
  }, 
  {
    field = {
      {43,44,45,46,47,48,49,50 },
      {42,21,22,23,24,25,26,51 },
      {41,20, 7, 8, 9,10,27,52 },
      {40,19, 6, 1, 2,11,28,53 },
      {39,18, 5, 4, 3,12,29,54 },
      {38,17,16,15,14,13,30,55 },
      {37,36,35,34,33,32,31,56 },
      { 0,63,62,61,60,59,58,57 },
    },
    title = "WHIRLPOOL",
  }, 
  {
    field = {
      { 0, 0, 0, 1, 1, 0, 0, 0 },
      { 0, 0, 3, 2, 2, 3, 0, 0 },
      { 0, 5, 4, 3, 3, 4, 5, 0 },
      { 7, 6, 5, 4, 4, 5, 6, 7 },
      { 8, 7, 6, 5, 5, 6, 7, 8 },
      { 9, 8, 7, 6, 6, 7, 8, 9 },
      {10, 9, 8, 7, 7, 8, 9,10 },
      { 0,10, 9, 8, 8, 9,10, 0 },
    },
    title = "PRISM",
  }, 
  {
    field = {
      { 7, 6, 5, 0, 0, 5, 6, 7 },
      { 6, 5, 4, 0, 0, 4, 5, 6 },
      { 5, 4, 3, 0, 0, 3, 4, 5 },
      { 4, 3, 2, 1, 1, 2, 3, 4 },
      { 4, 3, 2, 1, 1, 2, 3, 4 },
      { 5, 4, 3, 0, 0, 3, 4, 5 },
      { 6, 5, 4, 0, 0, 4, 5, 6 },
      { 7, 6, 5, 0, 0, 5, 6, 7 },
    },
    title = "EXPANDER",
  }, 
}

scenario.compileByOrder = function(mask)
  -- ensure that mission is ok for the configured board
  for y = 1, config.rows do
    if #mask < y then mask[y] = {} end
    for x = 1, config.cols do
      if #mask[y] < x then mask[y][x] = 0 end
    end
  end
  -- order playable cells ordered by 1..MAX (zeroes not included)
  local cells = {}
  for y = 1, config.rows do
    for x = 1, config.cols do
      if mask[y][x] > 0 then
        push(cells, { order = mask[y][x], x = x, y = y })
      end
    end
  end
  table.sort(cells, scenario.sorterByOrder)
  return cells
end

scenario.sorterByOrder = function(a, b)
  return a.order < b.order
end

scenario.__constructor = function()
  for _, holder in ipairs({ scenario.free, scenario.campaign }) do
    for _, mission in ipairs(holder) do
      mission.cells = scenario.compileByOrder(mission.field)
      if not mission.moves then mission.moves = -1 end
      if not mission.score then mission.score = -1 end
    end
  end
end
