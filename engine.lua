
engine.generate = function(mission)
  local filler = function(x, y, b)
    if mission.board then
      return mission.board[y][x] or 0
    end
    while true do
      local style = math.random(1, config.tiles)
      if not b:same(x-2, y, 1, 0, 2, style) and not b:same(x, y-2, 0, 1, 2, style) then
        return style
      end
    end
  end
  return board.new(config.cols, config.rows, mission.field, mission.cells,
    (not config.anim.initialAnimation) and filler or null,
    (not config.anim.initialAnimation) and function(b)
      return engine.getValidMove(b)
    end or null)
end

engine.getMatches = function(data)
  local matches = {}

  -- check horizontal
  for y = 1, data:rows() do
    local x = 1
    while x <= data:cols()-config.match+1 do
      local cell = data:value(x, y)
      local allSame = cell > 0
      if allSame then
        for i = 1, config.match-1 do
          if data:value(x+i,y) ~= cell then
            allSame = false
            break
          end
        end
      end
      if allSame then
        local match = {}
        push(matches, match)
        push(match, {x, y})
        for i = x+1, data:cols() do
          if data:value(i, y) == cell then
            push(match, {i, y})
            x = i
          else
            break
          end
        end
      end
      x = x + 1
    end
  end

  -- check for vertical
  for x = 1, data:cols() do
    local y = 1
    while y <= data:rows()-config.match+1 do
      local cell = data:value(x, y)
      local allSame = cell > 0
      if allSame then
        for i = 1, config.match-1 do
          if data:value(x,y+i) ~= cell then
            allSame = false
            break
          end
        end
      end
      if allSame then
        local match = {}
        push(matches, match)
        push(match, {x, y})
        for i = y+1, data:rows() do
          if data:value(x, i) == cell then
            push(match, {x, i})
            y = i
          else
            break
          end
        end
      end
      y = y + 1
    end
  end

  if #matches > 0 then
    -- process special effects
    local append = {}
    for _, match in ipairs(matches) do
      for _, rec in ipairs(match) do
        local cell = data:get(rec[1], rec[2])
        local s = cell:getS()
        if s == "hor" then
          for x = 1, data:cols() do
            push(append, { x, rec[2] })
          end
        elseif s == "ver" then
          for y = 1, data:rows() do
            push(append, { rec[1], y })
          end
        end
      end
    end
    
    -- append special cells, one-by-one
    for _, ap in ipairs(append) do
      local already = false
      for _, match in ipairs(matches) do
        for _, rec in ipairs(match) do
          if rec[1] == ap[1] and rec[2] == ap[2] then
            already = true
            break
          end
        end
      end
      if not already then
        push(matches, { ap })
      end
    end
  end

  return matches
end

engine.validCell = function(x, y, data)
  return x and y and data:mask(x, y) > 0
end

engine.validMove = function(x1, y1, x2, y2, data, ignoreMatches)
  if x1 == x2 and y1 == y2 then return false end
  if not engine.validCell(x1, y1, data) or not engine.validCell(x2, y2, data) then return false end
  -- only move by 1 cell
  local dx = math.abs(x1 - x2) 
  local dy = math.abs(y1 - y2) 
  if dx + dy ~= 1 then return false end
  -- deny stupid moves
  if ignoreMatches then return true end
  if data:value(x1, y1) == data:value(x2, y2) then return false end
  -- deny cheat moves (without matching anything)
  data:swap(x1, y1, x2, y2)
  local matches = engine.getMatches(data)
  data:swap(x1, y1, x2, y2)
  if #matches < 1 then return false end
  return true
end

engine.calcScore = function(matched)
  local score = 0
  for _, match in ipairs(matched) do
    score = score + config.score[#match]
  end
  return score
end

engine.getValidMove = function(data)
  for y = 1, data:rows() do
    for x = 1, data:cols() do
      if (y % 2 == 1 and x % 2 == 0) or (y % 2 == 0 and x %2 == 1) then
        if x > 1 and engine.validMove(x, y, x - 1, y, data) then
          return {x, y, x-1, y}
        elseif x < config.cols and engine.validMove(x, y, x + 1, y, data) then
          return {x, y, x+1, y}
        elseif y > 1 and engine.validMove(x, y, x, y - 1, data) then
          return {x, y, x, y-1}
        elseif y < config.rows and engine.validMove(x, y, x, y + 1, data) then
          return {x, y, x, y+1}
        end
      end
    end
  end
  return nil
end

engine.shuffle = function(data)
  for i = 1, data:cols() * data:rows() do
    local x1 = math.random(1, data:cols())
    local x2 = math.random(1, data:cols())
    local y1 = math.random(1, data:rows())
    local y2 = math.random(1, data:rows())
    if data:mask(x1, y1) > 0 and data:mask(x2, y2) > 0 then
      data:swap(x1, y1, x2, y2)
    end
  end
end
