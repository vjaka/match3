require("lib/lib")
loadModules({
    -- no state means no delegates, empty state means any suits
    { id = "menu", state = { "menu" } },
    { id = "level", state = { "level" } },
    { id = "hud", state = { "level" } },
    { id = "engine" },
    { id = "scenario" },
    { id = "scene" },
    { id = "ui", state = { } },
    { id = "options", state = { "options" } },
  })
loadClasses("cell", "board", "tileanim")
game = {
  active = true,
}

function love.resize(w, h)
  width, height = w, h
  scale = height / 1024
  delegate("onResize")
  timer = 0
  ticksPerSecond = 2
  math.randomseed(os.time())
end

love.load = function()
  if arg[#arg] == "-debug" then require("mobdebug").start() end
  love.resize(love.window.getWidth(), love.window.getHeight())
  love.window.setTitle("OpenGemmy")
  transition = 0
  scene.change("menu")
  delegate("onLoad")
end

love.draw = function()
  if not game.active then return end
  love.graphics.setBackgroundColor(48, 0, 64)
  love.graphics.setColor(255,255,255)
  delegate("onDraw")
  love.graphics.setColor(255,255,255)
  ui.button.draw()
end

local touches = {}

love.touchpressed = function(id, x, y, pressure)
  if transition > 0 then return end
  if ui.button.click(x, y) then return end
  table.insert(touches, {id = id, x = x, y = y})
  delegate("onMousePress", x*width, y*height, 1)
end

love.mousepressed = function(x, y, button)
  if transition > 0 then return end
  delegate("onMousePress", x, y, button)
end

love.touchreleased = function(id, x, y, pressure)
  if transition > 0 then return end
  local ignore = false
  for i = #touches, 1, -1 do
    if touches[i].moved then
      ignore = true
    end
    if touches[i].id == id then
      table.remove(touches, i)
    end
  end
  if not ignore then
    delegate("onMouseRelease", x*width, y*height, 1)
  end
end

love.touchmoved = function(id, x, y, pressure)
  if transition > 0 then return end
  for i, v in ipairs(touches) do
    if v.id == id and not v.moved then
      local xv = x - v.x
      local yv = y - v.y
      local axv = math.abs(xv)
      local ayv = math.abs(yv)
      local threshold = 0.08  -- only accepts move within a threshold
      if axv >= threshold or ayv >= threshold then
        if delegate("onMouseRelease", x*width, y*height, 1) then
          v.moved = true
        end
      end
      return
    end
  end
end

love.mousereleased = function(x, y, button)
  if transition > 0 then return end
  if ui.button.click(x, y) then return end
  delegate("onMouseRelease", x, y, button)
end

love.update = function(dt)
  if not game.active then return end
  local passedTick = math.floor((timer + dt)*ticksPerSecond) ~= math.floor((timer)*ticksPerSecond)
  if transition > 0 then
    transition = transition - 1
    if transition > 0 then return end
  end
  if passedTick then
    delegate("onTick", dt)
  end
  timer = timer + dt
  delegate("onUpdate", dt)
end

love.keypressed = function(key)
  if transition > 0 then return end
  delegate("onKeyPress", key)
end

love.keyreleased = function(key)
  if transition > 0 then return end
  delegate("onKeyRelease", key)
end

love.focus = function(focus)
  if transition > 0 then return end
  if game.active == focus then return end
  game.active = focus
  if focus then
    delegate("onFocus")
  else
    delegate("onLoseFocus")
  end
end
