ui.onResize = function()
  if not level.gfxMenu then level.gfxMenu = love.graphics.newImage("assets/btn_menu.png") end
  if not scene.fontHeading then scene.fontHeading = love.graphics.newFont(height / 28) end
  if not scene.fontText then scene.fontText = love.graphics.newFont(height / 55) end
  ui.button.onResize()
end

ui.button = {
  btns = {},

  add = function(id, rect, draw, click)
    local btn = { rect = rect, draw = draw, click = click }
    if type(btn.draw) ~= "function" then
      btn.draw = function(rect)
        love.graphics.setFont(scene.fontHeading)
        love.graphics.setColor(200, 200, 255, 255)
        love.graphics.rectangle("line", rect.x1, rect.y1, rect.x2-rect.x1, rect.y2-rect.y1)
        local x, y = love.mouse.getPosition()
        if x >= rect.x1 and x < rect.x2 and y >= rect.y1 and y < rect.y2 then
          love.graphics.setColor(255, 255, 255, 255)
        else
          love.graphics.setColor(255, 255, 255, 0.75*255)
        end
        local text = type(draw) == "table" and draw[1]() or draw
--        if type(text) ~= "string" then text = draw() end
        love.graphics.printf(text, rect.x1, (rect.y1+rect.y2)/2-scene.fontHeading:getHeight()/2, rect.x2-rect.x1, "center")
        love.graphics.setColor(255, 255, 255, 255)
      end
    end
    btn.r = rect()
    ui.button.btns[id] = btn
  end,

  get = function(x, y)
    for key, btn in pairs(ui.button.btns) do
      if x >= btn.r.x1 and x <= btn.r.x2 and y >= btn.r.y1 and y <= btn.r.y2 then
        return btn
      end
    end
  end,
  
  remove = function(id)
    ui.button.btns[id] = nil
  end,

  clear = function()
    ui.button.btns = {}
  end,

  onResize = function()
    for key, btn in pairs(ui.button.btns) do
      btn.r = btn.rect()
    end
  end,

  click = function(x, y)
    local btn = ui.button.get(x, y)
    if btn then
      btn.click()
      return true
    end
  end,

  draw = function()
    for key, btn in pairs(ui.button.btns) do
      btn.draw(btn.r)
    end
  end,
}
