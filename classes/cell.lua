-- cell:
-- v = value
-- x = x
-- y = y
-- s = special type
-- sanim = animation

cell = {
  new = function(x, y, value)
    local c = { x = x, y = y, v = value or 0 }
    return setmetatable(c, cell.meta)
  end,

  get = function(this)
    return this.v
  end,
  
  set = function(this, value)
    this.v = value
  end,

  getS = function(this)
    return this.s
  end,
  
  setS = function(this, s)
    this.s = s
  end,

  clear = function(this)
    this.v = 0
    this.s = nil
    this.sanim = nil
    this.mod = nil
  end,

  getSAnim = function(this)
    return this.sanim or 1
  end,

  nextSAnim = function(this, total)
    if not this.sanim or this.sanim >= total then
      this.sanim = 1
    else
      this.sanim = this.sanim + 1
    end
  end,

  setModified = function(this, value)
    this.mod = value
  end,

  getModified = function(this)
    return this.mod or 0
  end,
}
return cell