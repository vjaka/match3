-- board:
-- a = array data of cells (transposed to 1 dimension, use board.index(b,x,y) to calculate)
-- h = height (rows)
-- m = mask (if exists, [y][x] > 0 means that it contains the cell
-- w = width (columns)

board = {
  new = function(cols, rows, mask, maskCells, filler, validator)
    local b = {}
    b.w = cols
    b.h = rows
    b.m = mask
    b.mc = maskCells
    b.a = {}
    for y = 1, rows do
      b.a[y] = {}
      for x = 1, cols do
        b.a[y][x] = cell.new(x, y, 0)
      end
    end
    b = setmetatable(b, board.meta)
    if filler then
      local iter = 1
      repeat
        if iter > 1 and config.debug then
          print("retry #" .. tostring(iter) .. " filling table " .. tostring(cols) .. "x" .. tostring(rows))
        end
        for y = 1, rows do
          for x = 1, cols do
            if mask[y][x] > 0 then
              local val = filler(x, y, b)
              b.a[y][x]:set(val)
            end
          end
        end
        iter = iter + 1
      until (not validator) or validator(b)
    end
    return b
  end,

  cols = function(this)
    return this.w
  end,

  rows = function(this)
    return this.h
  end,

  get = function(this, x, y)
    if x < 1 or x > this:cols() or y < 1 or y > this:rows() then return nil end
    return this.a[y][x]
  end,

  value = function(this, x, y)
    local cell = this:get(x, y)
    if cell then
      return cell:get()
    end
    return -1
  end,

  clear = function(this, x, y)
    local cell = this:get(x, y)
    if cell and cell:get() > 0 then
      cell:clear()
      return true
    end
  end,

  same = function(this, x, y, dx, dy, len, exactMatch)
    local cell = this:get(x, y)
    if not cell then return false end
    if exactMatch and cell:get() ~= exactMatch then return false end
    for i = 2, len do
      local x2 = x + dx * (len - 1)
      local y2 = y + dy * (len - 1)
      local cell2 = this:get(x2, y2)
      if not cell2 then return false end
      if exactMatch then
        if cell2:get() ~= exactMatch then return false end
      else
        if cell2:get() ~= cell:get() then return false end
      end
    end
    return true
  end,

  set = function(this, x, y, cell)
    if x >= 1 and x <= this:cols() and y >= 1 and y <= this:rows() then
      this.a[y][x] = cell
      cell.x = x
      cell.y = y
      return true
    end
  end,

  swap = function(this, x1, y1, x2, y2)
    local c1 = this:get(x1, y1)
    local c2 = this:get(x2, y2)
    if c1 and c2 then
      this:set(x1, y1, c2)
      this:set(x2, y2, c1)
      return true
    end
  end,

  mask = function(this, x, y)
    if not this.m then return 1 end
    return this.m[y][x]
  end,

  maskCells = function(this)
    return this.mc
  end
}
return board
