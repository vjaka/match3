-- tileanim:
-- source { x, y }
-- from { x, y, alpha }
-- to { x, y, alpha }
-- time
-- TODO: make tile animation class, replace level.anim operations with list of this

tileanim = {
  new = function(x, y, time, from, to, delay)
    if not from then from = { x, y, 1 } end
    if not to then to = from or { x, y, 1 } end
    local a = { source = { x, y }, from = from, to = to, time = time, delay = delay }
    return a
  end,

  setFinish = function(this, finish)
    this.finish = finish
    return this
  end,
}
return tileanim
