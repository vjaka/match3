menu.onKeyPress = function(key)
  if key == "escape" then
    love.event.quit()
  end
end

menu.onOpen = function()
  ui.button.add("exit",
    function()
      local menuPart = width * config.ui.wmenu
      local exitW = menuPart*0.6
      local exitH = (level.gfxMenu:getHeight() / level.gfxMenu:getWidth()) * exitW
      return { x1 = 0, y1 = height - exitH, x2 = exitW, y2 = height }
    end,
    function(rect)
      love.graphics.draw(level.gfxMenu, rect.x1, rect.y1, 0,
        (rect.x2-rect.x1)/level.gfxMenu:getWidth(), (rect.y2-rect.y1)/level.gfxMenu:getHeight())
    end,
    function()
      love.event.quit()
    end)
--  ui.button.add("campaign",
--    function()
--      return { x1 = width/4, y1 = height/6, x2 = width/4*3, y2 = height/6*2 }
--    end,
--    "Campaign",
--    function()
--      scene.change("level", scenario.campaign[1])
--    end)
  ui.button.add("freeplay",
    function()
      return { x1 = width/4, y1 = height/6*2, x2 = width/4*3, y2 = height/6*3 }
    end,
    "Free Play",
    function()
      scene.change("level", scenario.free[1])
    end)
  ui.button.add("randomfreeplay",
    function()
      return { x1 = width/4, y1 = height/6*3, x2 = width/4*3, y2 = height/6*4 }
    end,
    "Random Free Play",
    function()
      scene.change("level", scenario.free[math.random(2, #scenario.free)])
    end)
  ui.button.add("options",
    function()
      return { x1 = width/4, y1 = height/6*4, x2 = width/4*3, y2 = height/6*5 }
    end,
    "Options",
    function()
      scene.change("options")
    end)

end
