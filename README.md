# README #

Free, open-source match3 game in LUA.
Available as a compiled application on [Android](https://play.google.com/store/apps/details?id=org.opengemmy)

### Requirements ###

* [Love2D](https://bitbucket.org/rude/love) game engine
* Any LUA editor (for example, [ZeroBrane Studio](https://github.com/pkulchenko/ZeroBraneStudio))
