level.load = function(mission)
  level.gfxScore = love.graphics.newImage("assets/panel_score.png")
  if not level.gfxMenu then level.gfxMenu = love.graphics.newImage("assets/btn_menu.png") end
  level.gfxPanel = love.graphics.newImage("assets/panel.png")
  level.gfxUnused = love.graphics.newImage("assets/cell_unused.png")
  if not level.gfx then
    level.gfx = {}
    for i = 1, config.tiles do
      level.gfx[i] = love.graphics.newImage("assets/crystal" .. i .. ".png")
    end
  end
  if not level.gfxa then
    level.gfxa = {}
    for key, name in pairs({ ["hor"] = "hor", ["ver"] = "vert" }) do
      local img = love.graphics.newImage("assets/anim_"..name..".png")
      local quads = {}
      for i = 1, img:getWidth()/122 do
        local x = (i - 1) * 122
        push(quads, love.graphics.newQuad(x, 0, 122, 122, img:getWidth(), img:getHeight()))
        level.gfxa[key] = { i = img, q = quads }
      end
    end
  end
  if not level.animK then level.animK = 1 end
  level.score = 0
  level.moves = 0
  level.onResize()
  level.mission = mission
  level.table = engine.generate(level.mission)
  level.anims = {}

  ui.button.add("menu",
    function()
      local menuPart = width * config.ui.wmenu
      local exitW = menuPart*0.6
      local exitH = (level.gfxMenu:getHeight() / level.gfxMenu:getWidth()) * exitW
      return { x1 = 0, y1 = height - exitH, x2 = exitW, y2 = height }
    end,
    function(rect)
      love.graphics.draw(level.gfxMenu, rect.x1, rect.y1, 0,
        (rect.x2-rect.x1)/level.gfxMenu:getWidth(), (rect.y2-rect.y1)/level.gfxMenu:getHeight())
    end,
    function()
      scene.change("menu")
    end)
  ui.button.add("suggest",
    function()
      local h = height
      local menuPart = width * config.ui.wmenu
      local w = menuPart
      return { x1 = w/10*2, y1 = h/10*4, x2 = w/10*8, y2 = h/10*6 }
    end,
    "Suggest",
    function()
      if #level.anims > 0 then return end
      local suggest = engine.getValidMove(level.table)
      if suggest then
        level.score = level.score - config.suggestCostScore
        for i = 1, config.suggestAnimRepeat do
          push(level.anims, {
              source = { suggest[1], suggest[2] },
              from = { suggest[1], suggest[2], 1 },
              to = { suggest[1]-0.1, suggest[2]-0.1, 0.5 },
              delay = i*config.suggestAnimTime*level.animK,
              time = config.suggestAnimTime*level.animK,
              })
          push(level.anims, {
              source = { suggest[1], suggest[2] },
              from = { suggest[1]-0.1, suggest[2]-0.1, 0.5 },
              to = { suggest[1], suggest[2], 1 },
              delay = i*config.suggestAnimTime*level.animK*2,
              time = config.suggestAnimTime*level.animK,
            })
          
          push(level.anims, {
              source = { suggest[3], suggest[4] },
              from = { suggest[3], suggest[4], 1 },
              to = { suggest[3]+0.1, suggest[4]+0.1, 0.5 },
              delay = i*config.suggestAnimTime*level.animK,
              time = config.suggestAnimTime*level.animK,
              })
          push(level.anims, {
              source = { suggest[3], suggest[4] },
              from = { suggest[3]+0.1, suggest[4]+0.1, 0.5 },
              to = { suggest[3], suggest[4], 1 },
              delay = i*config.suggestAnimTime*level.animK*2,
              time = config.suggestAnimTime*level.animK,
              })
          end
      end
    end)

  if config.anim.initialAnimation then
    level.processDroppingTiles(true)
  end
end

level.onDraw = function()
  love.graphics.setBackgroundColor(48, 0, 64)

  local menuPart = width/4
  local scoreW = menuPart
  local scoreH = (level.gfxScore:getHeight() / level.gfxScore:getWidth()) * scoreW
  love.graphics.draw(level.gfxScore, 0, 0, 0, scoreW/level.gfxScore:getWidth(), scoreH/level.gfxScore:getHeight())

  love.graphics.draw(level.gfxPanel, level.rect.x1-level.tile_xsize, level.rect.y1-level.tile_ysize,
    0,
    level.tile_xsize*(config.cols+1.58)/level.gfxPanel:getWidth(),
    level.tile_ysize*(config.rows+1.516393443)/level.gfxPanel:getHeight()
  )

  local font = love.graphics.getFont()
  if not level.font then level.font = love.graphics.newFont(math.ceil(height/28)) end
  love.graphics.setFont(level.font)
  love.graphics.printf("SCORE:\n " .. tostring(level.score), height/28, height/28/2, width)
  if level.mission.moves > 0 then
    love.graphics.printf("MOVES: " .. tostring(level.mission.moves - level.moves),
      level.rect.x1, level.rect.y1-level.tile_ysize/2, level.rect.x2-level.rect.x1, "left")
  end
  if level.mission.score > 0 then
    love.graphics.printf("TARGET: " .. tostring(level.mission.score),
      level.rect.x1, level.rect.y1-level.tile_ysize/2, level.rect.x2-level.rect.x1, "right")
  end
  if level.mission.title then
    love.graphics.printf(level.mission.title,
      level.rect.x1, level.rect.y1-level.tile_ysize/2, level.rect.x2-level.rect.x1, "center")
  end
  love.graphics.setFont(font)

  -- layer 1: background
  for y = 1, config.rows do
    for x = 1, config.cols do
      local cellX = level.rect.x1+(x-1)*level.tile_xsize
      local cellY = level.rect.y1+(y-1)*level.tile_ysize
      if level.mission.field[y][x] < 1 then
        love.graphics.draw(level.gfxUnused, cellX, cellY, 0,
          level.tile_xsize/level.gfxUnused:getWidth(), level.tile_ysize/level.gfxUnused:getHeight()
        )
      end
    end
  end

  -- layer 2: tiles
  for y = 1, config.rows do
    for x = 1, config.cols do
      if not level.getAnimation(x, y) then
        local cellX = math.floor(level.rect.x1+(x-1)*level.tile_xsize)
        local cellY = math.floor(level.rect.y1+(y-1)*level.tile_ysize)
        level.drawTile(level.table:get(x, y), cellX, cellY)
      end
    end
  end
  
  for _, anim in ipairs(level.anims) do
    if anim.start then
      local progress = anim.time/anim.duration
      local dx = anim.to[1] - anim.from[1]
      local dy = anim.to[2] - anim.from[2]
      local x = anim.to[1] - dx*progress
      local y = anim.to[2] - dy*progress
      local cellX = math.floor(level.rect.x1+(x-1)*level.tile_xsize)
      local cellY = math.floor(level.rect.y1+(y-1)*level.tile_ysize)
      local fromAlpha = anim.from[3]
      local toAlpha = anim.to[3]
      if fromAlpha and toAlpha then
        local alpha = toAlpha - (toAlpha - fromAlpha) * progress
        love.graphics.setColor(255, 255, 255, math.ceil(alpha * 255))
      end
      level.drawTile(level.table:get(anim.source[1], anim.source[2]), cellX, cellY)
    end
  end
end

level.drawTile = function(cell, x, y)
  local tile = level.gfx[cell:get()]
  if not tile then return end
  local sx = level.tile_xsize/tile:getWidth()
  local sy = level.tile_ysize/tile:getHeight()
  love.graphics.draw(tile, x, y, 0, sx, sy)
  local spec = cell:getS()
  if spec then
    local anim = level.gfxa[spec]
    if anim then
      local quad = anim.q[cell:getSAnim()]
      love.graphics.draw(anim.i, quad, x, y, 0, sx, sy)
    end
  end
end

level.fromScreenToOffset = function(x, y)
  local xx, yy
  if x >= level.rect.x1 and x <= level.rect.x2 then
    xx = math.ceil((x-level.rect.x1)/level.tile_xsize)
  end
  if y >= level.rect.y1 and y <= level.rect.y2 then
    yy = math.ceil((y-level.rect.y1)/level.tile_ysize)
  end
  if xx and yy and xx>0 and yy>0 then return xx, yy end
  return nil,nil
end

level.onMousePress = function(x, y, button)
  if #level.anims > 0 then return end
  local xx, yy = level.fromScreenToOffset(x, y)
  if engine.validCell(xx, yy, level.table) then
    level.drag = { x = xx, y = yy }
  end
end

level.onMouseRelease = function(x, y, button)
  if #level.anims > 0 then return end
  local xx, yy = level.fromScreenToOffset(x, y)
  if not level.drag then return end
  if level.mission.moves > 0 and level.moves >= level.mission.moves then return end
  if engine.validMove(level.drag.x, level.drag.y, xx, yy, level.table, true) then
    if engine.validMove(level.drag.x, level.drag.y, xx, yy, level.table) then
      push(level.anims, {
          source = { xx, yy },
          from = { xx, yy },
          to = { level.drag.x, level.drag.y },
          time = config.anim.move_1cell*level.animK,
          finish = function()
            level.table:swap(level.drag.x, level.drag.y, xx, yy)
            level.table:get(level.drag.x, level.drag.y):setModified(level.moves)
            level.table:get(xx, yy):setModified(level.moves)
            level.matchCells()
          end })
      push(level.anims, {
          source = { level.drag.x, level.drag.y },
          from = { level.drag.x, level.drag.y },
          to = { xx, yy },
          time = config.anim.move_1cell*level.animK })
      level.moves = level.moves + 1
    else
      push(level.anims, {
          source = { level.drag.x, level.drag.y },
          from = { level.drag.x, level.drag.y },
          to = { xx, yy },
          time = config.anim.move_1cell*level.animK })
      push(level.anims, {
          source = { level.drag.x, level.drag.y },
          from = { xx, yy },
          to = { level.drag.x, level.drag.y },
          delay = config.anim.move_1cell*level.animK,
          time = config.anim.move_1cell*level.animK })
    end
    return true
  end
end

level.onResize = function()
  local menuPart = width * config.ui.wmenu
  local fieldW = math.min(width - menuPart, height)
  local fieldH = fieldW
  level.tile_xsize = fieldW / (config.cols + 1)
  level.tile_ysize = fieldH / (config.rows + 1)
  local tableW = width - menuPart - (1+level.tile_xsize)*config.cols
  local tableH = height - (1+level.tile_ysize)*config.rows
  local x1 = menuPart + tableW/2
  local y1 = tableH/2 + level.tile_ysize/10*3
  level.rect = { x1 = x1, y1 = y1,
    x2 = x1 + level.tile_xsize*config.cols,
    y2 = y1 + level.tile_ysize*config.rows }
end

level.matchCells = function(initial)
  local matched = engine.getMatches(level.table)
  if #matched < 1 then return end
  
  level.score = initial and level.score or (level.score + engine.calcScore(matched))

  -- clear
  local lastDisappearAnim
  for _, match in ipairs(matched) do
    local matchedN = #match
    local matchedHor = match[2] and match[1][2] == match[2][2]
    local latest = 0
    local latestI
    for i, rec in ipairs(match) do
      local mod = level.table:get(rec[1], rec[2]):getModified()
      if mod > latest then
        latest = mod
        latestI = i
      end
    end
    for i, rec in ipairs(match) do
      if level.table:value(rec[1], rec[2]) ~= -1 then
        local shouldRemove = true
        
        if matchedN == config.specialMatch and matchedHor and i == latestI then
          level.table:get(rec[1], rec[2]):setS("hor")
          shouldRemove = false
        elseif matchedN == config.specialMatch and not matchedHor and i == latestI then
          level.table:get(rec[1], rec[2]):setS("ver")
          shouldRemove = false
        end

        push(level.anims, {
            source = { rec[1], rec[2] },
            from = { rec[1], rec[2], 1 },
            to = { rec[1]-0.2, rec[2], 0.8 },
            time = config.anim.disappear/4*level.animK })
        push(level.anims, {
            source = { rec[1], rec[2] },
            from = { rec[1]-0.2, rec[2], 0.8 },
            to = { rec[1], rec[2]-0.2 }, 0.6,
            delay = config.anim.disappear/4*level.animK,
            time = config.anim.disappear/4*level.animK })
        push(level.anims, {
            source = { rec[1], rec[2] },
            from = { rec[1], rec[2]-0.2, 0.6 },
            to = { rec[1]+0.2, rec[2], 0.4 },
            delay = config.anim.disappear/4*2*level.animK,
            time = config.anim.disappear/4*level.animK })
        lastDisappearAnim = {
            source = { rec[1], rec[2] },
            from = { rec[1]+0.2, rec[2], 0.4 },
            to = { rec[1], rec[2]+0.2, 0.2 },
            delay = config.anim.disappear/4*3*level.animK,
            time = config.anim.disappear/4*level.animK,
            finish = function()
              if shouldRemove then
                level.table:clear(rec[1], rec[2])
              end
            end }
        push(level.anims, lastDisappearAnim)
      end
    end
  end
  lastDisappearAnim.time = lastDisappearAnim.time + 0.05
  lastDisappearAnim.extraFinish = function()
    level.processDroppingTiles(initial)
  end

end

level.processDroppingTiles = function(initial)
  local lastStealAnim
  local cells = level.table:maskCells()
  local animK = initial and config.anim.initial_k or level.animK

  -- try to drop cell by cell and spawn
  for i = #cells, 1, -1 do
    local mcell = cells[i]
    local empty = level.table:value(mcell.x, mcell.y) < 1
    if empty and mcell.order == 1 then
      lastStealAnim = {
        source = { mcell.x, mcell.y },
        from = { mcell.x, mcell.y-1, 0.3 },
        to = { mcell.x, mcell.y, 1 },
        time = config.anim.drop_1cell*animK,
      }
      push(level.anims, lastStealAnim)
      level.table:get(mcell.x, mcell.y):set(math.random(1, config.tiles))
    elseif empty and level.table:value(mcell.x, mcell.y) < 1 then
      local test = { { x = mcell.x - 1, y = mcell.y},
        { x = mcell.x, y = mcell.y - 1 },
        { x = mcell.x + 1, y = mcell.y },
        { x = mcell.x, y = mcell.y + 1 } }
      for _, t in ipairs(test) do
        if t.x >= 1 and t.x <= level.table:cols() and t.y >= 1 and t.y <= level.table:rows() then
          local thatOrder = level.table:mask(t.x, t.y)
          if thatOrder > 0 and mcell.order == thatOrder + 1 and level.table:value(t.x, t.y) > 0 and not level.table:get(t.x, t.y).stolen then
            level.table:get(t.x, t.y).stolen = true
            -- steal it
            lastStealAnim = {
              source = { t.x, t.y },
              from = { t.x, t.y },
              to = { mcell.x, mcell.y },
              time = config.anim.drop_1cell*animK,
              finish = function()
                level.table:get(t.x, t.y).stolen = nil
                level.table:swap(mcell.x, mcell.y, t.x, t.y)
              end}
            push(level.anims, lastStealAnim)
            break
          end
        end
      end
    end
  end

  if lastStealAnim then
    lastStealAnim.time = lastStealAnim.time + 0.05
    lastStealAnim.extraFinish = function()
      level.processDroppingTiles(initial)
    end
  else
    if not engine.getValidMove(level.table) then
      -- resort the table
      engine.shuffle(level.table)
      transition = 2
    end
    level.matchCells(initial)
  end
end

level.onTick = function(dt)
  if #level.anims > 0 then return end
end

level.onUpdate = function(dt)
  if timer - (level.animUpdate or 0) > config.anim.tile_anim * level.animK then
    for y = 1, config.rows do
      for x = 1, config.cols do
        local cell = level.table:get(x, y)
        if cell:getS() then
          cell:nextSAnim(#level.gfxa[cell:getS()].q)
        end
      end
    end
    level.animUpdate = timer
  end
  if #level.anims < 1 then return end
  for i = #level.anims, 1, -1 do
    local anim = level.anims[i]
    if (anim.delay or 0) > 0 then
      anim.delay = anim.delay - dt
    end
    if (anim.delay or 0) <= 0 then
      anim.delay = nil
    end
    if anim.start then
      anim.time = anim.time - dt
      if anim.time <= 0 then
        table.remove(level.anims, i)
        if anim.finish then
          anim.finish()
        end
        -- TODO: replace extraFinish by some callback like all animations finished
        -- to remove the requirement to slowdown last animation in time to achieve
        -- the desired effects
        if anim.extraFinish then
          anim.extraFinish()
        end
      end
    elseif not anim.delay then
      anim.start = timer
      anim.duration = anim.time
    end
  end
end

level.getAnimation = function(x, y)
  for _, anim in ipairs(level.anims) do
    if anim.start and anim.source[1] == x and anim.source[2] == y then return anim end
  end
  return nil
end

level.onKeyPress = function(key)
  if #level.anims > 0 then return end
  if key == "escape" then
    scene.change("menu")
  end
end
