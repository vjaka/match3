scene.change = function(target, ...)
  ui.button.clear()
  state = target
  transition = 2

  if target == "menu" then
    menu.onOpen()
  elseif target == "level" then
    level.load(...)
  elseif target == "options" then
    options.onOpen()
  end

end
